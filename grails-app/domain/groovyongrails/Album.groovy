package groovyongrails

class Album {
String title

    static constraints = {
        title()
    }

    static mapping = {
      table 'Albums'
        version false
        columns{
            title column: 'Titlessss'
        }
    }

}
