package groovyongrails

class BootStrap {

    def init = { servletContext ->
        def riverside = new Artist(name: 'riverside')
        riverside.addToAlbums(title: 'Love, Fear and the time machine')
        riverside.save()

        def davidBowie = new Artist(name:'Dawid Bowie')
        davidBowie.addToAlbums(title: 'Space Oddity')

    }
    def destroy = {
    }
}
