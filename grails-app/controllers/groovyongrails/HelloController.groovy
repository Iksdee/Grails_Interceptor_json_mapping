package groovyongrails

class HelloController {

    def index(){
        render (view:'hi.gsp')
    }

    def by() {

    }

    def hi(){

    }

    def list(){

        def list = []
        Album albumJ = new Album(title: 'Moon')
        Artist artist1 = new Artist(name: 'John', albums: albumJ)

        Album albumO = new Album(title: 'Space')
        Artist artist2 = new Artist(name: 'Olivier', albums: albumO )

        list.add(artist1)
        list.add(artist2)

        [list:list]

    }
}
