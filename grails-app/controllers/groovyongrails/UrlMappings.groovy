package groovyongrails

class UrlMappings {

    static mappings = {
        group("/hello"){
            "/report/$action?/$id?(.$format)?"(controller: 'hello')
        }
        group("/person"){
            "/add/$action?/$id?(.$format)?"(controller: 'person')
        }

        // (http://localhost:8080/hello/report/list)
        // (http://localhost:8080/hello/report/by)
        // https://github.com/pfoletto


        "/"(view:"/index")
        "/hi"(view: '/by')
        "500"(view:'/error')
        "404"(view:'/notFound')
    }
}
