package groovyongrails

class SecondController {

    def alpha() {
        render "render by SecondController.alpha"
    }

    def beta(){
        render "render by SecondController.beta"
    }
}
