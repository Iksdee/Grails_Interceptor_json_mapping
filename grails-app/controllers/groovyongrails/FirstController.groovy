package groovyongrails

class FirstController {

    def alpha() {
        render "render by FirstController.alpha"
    }

    def beta(){
        render "render by FirstController.beta"
    }

}
