package groovyongrails


class MainInterceptor {

    MainInterceptor() {
        match controller: 'first', action: 'alpha'
        match controller: 'second', action: 'beta'
    }

    boolean before() {
        log.debug 'Something new....'
        if(params.specialParam == 'Y'){
            render 'The inceptor dandled the request......'
            return false
        }
        true
    }
}
